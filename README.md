Ontology to formally describe smart objects and their capabilities.

This ontology has been derived by adopting and estending FIESTA-IoT [1] and the Physics Domain Ontology [2].

~

References:

[1] - Agarwal, R., Fernandez, D.G., Elsaleh, T., Gyrard, A., Lanza, J., Sanchez, L., Georgantas, N., Issarny, V.: Unified iot ontology to enable interoperability and federation of testbeds. In: WF-IoT 2016, pp. 70-75. IEEE Computer Society (2016)

[2] - Hachem, S., Teixeira, T., Issarny, V.: Ontologies for the internet of things. In: MDS'11, pp. 3:1-3:6. ACM (2011)